package top.pitaya.message;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author MR.HYPER
 * @version 1.0
 * @date 2022/04/12 1:48
 * @description
 */
@Service
public class Sender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void sendMsg(Map msg) {
        this.amqpTemplate.convertAndSend("evaluationQueue", msg);
    }
}
