package top.pitaya.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.pitaya.entity.CarEvaluationRefEntity;
import top.pitaya.entity.EvaluationEntity;
import top.pitaya.entity.Page;
import top.pitaya.entity.UserEvaluationRefEntity;

import java.util.List;
import java.util.Map;

@Mapper
public interface EvaluationMapper {
    EvaluationEntity getEvaluationInfo(@Param("id") String id);

    Integer createEvaluation(EvaluationEntity evaluationEntity);

    EvaluationEntity getEvaluationInfo(EvaluationEntity evaluationEntity);

    Page getTotalPageAndSize(Map paramMap);

    List<EvaluationEntity> getEvaluationByPage(Map paramMap);

    Integer updateEvaluation(EvaluationEntity evaluationEntity);

    Integer deleteEvaluation(String id);

    Integer createCarEvaluationRef(CarEvaluationRefEntity carRefEntity);

    Integer createUserEvaluationRef(UserEvaluationRefEntity userRefEntity);

    Integer updateEvaluationStatus(Map map);
}
