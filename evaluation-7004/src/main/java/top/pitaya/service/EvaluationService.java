package top.pitaya.service;

import top.pitaya.entity.CommonResult;
import top.pitaya.entity.EvaluationEntity;
import top.pitaya.entity.Page;

import java.util.Map;


public interface EvaluationService {
    EvaluationEntity getEvaluationInfo(String id);

    CommonResult createEvaluation(EvaluationEntity evaluationEntity);

    EvaluationEntity getEvaluationInfo(EvaluationEntity evaluationEntity);

    Page<EvaluationEntity> getEvaluationByPage(Integer currentPage, Integer pageSize, String fuzzy, String id);

    Integer updateEvaluationInfo(EvaluationEntity evaluationEntity);

    Integer deleteEvaluation(String id);

    Integer updateEvaluationStatus(String id);
}
