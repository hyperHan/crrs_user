package top.pitaya.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.UserEntity;


@Component
@FeignClient(value = "user-service")
public interface UserFeignService {
    // Feign解析不了参数，因此必须要指定value

    @GetMapping("/user/{id}")
    public CommonResult<UserEntity> getUserInfo(@PathVariable("id") String id);
}
