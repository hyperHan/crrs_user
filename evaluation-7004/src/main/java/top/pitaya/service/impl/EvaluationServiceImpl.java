package top.pitaya.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.pitaya.dao.EvaluationMapper;
import top.pitaya.entity.*;
import top.pitaya.message.Sender;
import top.pitaya.service.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class EvaluationServiceImpl implements EvaluationService {

    @Resource
    private EvaluationMapper evaluationMapper;

    @Resource
    private CommonService commonService;

    @Resource
    private CarFeignService carFeignService;

    @Resource
    private OrderFeignService orderFeignService;

    @Resource
    private UserFeignService userFeignService;

    @Resource
    private Sender messageSender;

    @Override
    public EvaluationEntity getEvaluationInfo(String id) {
        return evaluationMapper.getEvaluationInfo(id);
    }

    @Override
    public CommonResult createEvaluation(EvaluationEntity evaluationEntity) {
        String uuid = commonService.getUuid();
        evaluationEntity.setId(uuid);
        Integer evaluationCount = evaluationMapper.createEvaluation(evaluationEntity);
        // 新增用户评价关系记录
        Integer userRefCount = createUserEvaluationRef(uuid, evaluationEntity.getIdUser());
        // 新增汽车评价关系记录
        Integer carRefCount = createCarEvaluationRef(uuid, evaluationEntity.getIdCar());
        if (evaluationCount > 0 && userRefCount > 0 && carRefCount > 0) {
            // 调用RabbitMQ消息队列, 通知订单服务，将状态修改为已评价
            Map map = new HashMap();
            map.put("status","05");
            map.put("id",evaluationEntity.getIdOrder());
            messageSender.sendMsg(map);
            return new CommonResult("200", "新增成功！", uuid);
        } else {
            return new CommonResult("444", "新增失败");
        }
    }

    private Integer createCarEvaluationRef(String uuid, String idCar) {
        String id = commonService.getUuid();
        CarEvaluationRefEntity carRefEntity = new CarEvaluationRefEntity();
        carRefEntity.setIdEvaluation(uuid);
        carRefEntity.setIdCar(idCar);
        carRefEntity.setId(id);
        return evaluationMapper.createCarEvaluationRef(carRefEntity);
    }

    private Integer createUserEvaluationRef(String uuid, String idUser) {
        String id = commonService.getUuid();
        UserEvaluationRefEntity userRefEntity = new UserEvaluationRefEntity();
        userRefEntity.setIdEvaluation(uuid);
        userRefEntity.setIdUser(idUser);
        userRefEntity.setId(id);
        return evaluationMapper.createUserEvaluationRef(userRefEntity);
    }


    @Override
//    @HystrixCommand(fallbackMethod = "dealFallback", commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")})
    public EvaluationEntity getEvaluationInfo(EvaluationEntity evaluationEntity) {
        return evaluationMapper.getEvaluationInfo(evaluationEntity);
    }

    @Override
    public Page<EvaluationEntity> getEvaluationByPage(Integer currentPage, Integer pageSize, String fuzzy, String idUser) {
        Page<EvaluationEntity> page = new Page<>();
        page.setCurrentPage(currentPage);
        page.setPageSize(pageSize);
        // 获取总页数和总记录数
        Map paramMap = new HashMap();
        paramMap.put("pageSize", pageSize);
        paramMap.put("fuzzy", fuzzy);
        paramMap.put("idUser", idUser);
        Page pageTemp = evaluationMapper.getTotalPageAndSize(paramMap);
        page.setTotalSize(pageTemp.getTotalSize());
        page.setTotalPage(pageTemp.getTotalPage());
        // 获取分页列表
        paramMap.put("currentSize", (currentPage - 1) * pageSize);
        List<EvaluationEntity> list = evaluationMapper.getEvaluationByPage(paramMap);
        // 根据订单ID去关联表查询相关用户和汽车信息
        for (EvaluationEntity entity : list) {
            // 获取用户服务 用户信息
            getUserInfo(entity);
            // 获取汽车服务 汽车信息
            getCarInfo(entity);
            // 获取订单服务 订单信息
            getOrderInfo(entity);
        }
        page.setEntityList(list);
        return page;
    }

    private void getOrderInfo(EvaluationEntity entity) {
        CommonResult<OrderEntity> orderResult = orderFeignService.getOrderInfo(entity.getIdOrder());
        OrderEntity orderEntity = orderResult.getData();
        entity.setOrderNo(orderEntity.getOrderNo());
        entity.setDateStart(orderEntity.getDateStart());
        entity.setDateEnd(orderEntity.getDateEnd());
    }

    private void getCarInfo(EvaluationEntity entity) {
        CommonResult<CarEntity> carResult = carFeignService.getCarInfo(entity.getIdCar());
        CarEntity carEntity = carResult.getData();
        entity.setCarSeries(carEntity.getCarSeries());
        entity.setBrand(carEntity.getBrand());
        entity.setCarType(carEntity.getCarType());
        entity.setCarTypeName(carEntity.getCarTypeName());
        entity.setGearbox(carEntity.getGearbox());
        entity.setGearboxName(carEntity.getGearboxName());
        entity.setSeats(carEntity.getSeats());
        entity.setGears(carEntity.getGears());
        entity.setSweptVolume(carEntity.getSweptVolume());
        entity.setPrice(carEntity.getPrice());
    }

    private void getUserInfo(EvaluationEntity entity) {
        CommonResult<UserEntity> userResult = userFeignService.getUserInfo(entity.getIdUser());
        UserEntity userEntity = userResult.getData();
        entity.setUsername(userEntity.getUsername());
        entity.setName(userEntity.getName());
    }

    @Override
    public Integer updateEvaluationInfo(EvaluationEntity evaluationEntity) {
        return evaluationMapper.updateEvaluation(evaluationEntity);
    }

    @Override
    public Integer deleteEvaluation(String id) {
        return evaluationMapper.deleteEvaluation(id);
    }

    @Override
    public Integer updateEvaluationStatus(String id) {
        Map map = new HashMap();
        map.put("id", id);
        map.put("status", "09");
        return evaluationMapper.updateEvaluationStatus(map);
    }

    public String dealFallback() {
        return "系统繁忙，请稍后再试" + "\t" + "o(╥﹏╥)o";
    }

}
