package top.pitaya.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.pitaya.entity.CarEntity;
import top.pitaya.entity.CommonResult;


@Component
@FeignClient(value = "car-service")
public interface CarFeignService {

    @GetMapping("/car/{id}")
    public CommonResult<CarEntity> getCarInfo(@PathVariable("id") String id);
}
