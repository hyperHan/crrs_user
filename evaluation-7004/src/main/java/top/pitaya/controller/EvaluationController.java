package top.pitaya.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.EvaluationEntity;
import top.pitaya.entity.Page;
import top.pitaya.message.Sender;
import top.pitaya.service.EvaluationService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class EvaluationController {

    @Resource
    private EvaluationService evaluationService;

    @Resource
    private Sender sender;

    /**
     * 评价管理、我的评价分页查询
     */
    @GetMapping("/evaluation")
    public Map getEvaluationByPage(
            @RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "fuzzy", defaultValue = "") String fuzzy,
            @RequestParam(value = "id", defaultValue = "") String id) {
        // 获取评价列表
        Page<EvaluationEntity> page = evaluationService.getEvaluationByPage(currentPage, pageSize, fuzzy, id);
        Map result = new HashMap();
        result.put("data", page);
        if (page.getEntityList() != null && page.getEntityList().size() > 0) {
            result.put("code", "200");
            result.put("msg", "查询成功！");
        } else {
            result.put("code", "444");
            result.put("msg", "未查询到相关数据！");
        }
        return result;
    }

    /**
     * 用户评价
     *
     * @param evaluationEntity
     * @return
     */
    @PostMapping(value = "/evaluation")
    public CommonResult createEvaluation(@RequestBody EvaluationEntity evaluationEntity) {
        log.info("待插入评价数据：", evaluationEntity);
        return evaluationService.createEvaluation(evaluationEntity);
    }

    /**
     * 评价管理修改功能
     */
    @PutMapping(value = "/evaluation")
    public CommonResult updateEvaluation(@RequestBody EvaluationEntity evaluationEntity) {
        Integer count = evaluationService.updateEvaluationInfo(evaluationEntity);
        if (count > 0) {
            return new CommonResult("200", "修改成功！");
        } else {
            return new CommonResult("444", "修改失败！");
        }
    }

    /**
     * 评价管理删除功能
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/evaluation")
    public CommonResult deleteEvaluation(@RequestParam String id) {
        log.info("待删除的评价数据ID：" + id);
        Integer count = evaluationService.deleteEvaluation(id);
        if (count > 0) {
            return new CommonResult("200", "删除成功!");
        } else {
            return new CommonResult("444", "删除失败!");
        }
    }

    /**
     * 我的评价删除功能（假删除）
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/per/evaluation")
    public CommonResult deletePerEvaluation(@RequestParam String id) {
        log.info("待删除的评价数据ID：" + id);
        Integer count = evaluationService.updateEvaluationStatus(id);
        if (count > 0) {
            return new CommonResult("200", "删除成功!");
        } else {
            return new CommonResult("444", "删除失败!");
        }
    }

    /**
     * 测试 rabbitmq消息
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/msg")
    public void sendMsg(@RequestParam String id) {
        log.info("待传送的数据ID：" + id);
        Map map = new HashMap();
        map.put("id",id);
        sender.sendMsg(map);
    }

}
