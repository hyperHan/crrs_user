package top.pitaya.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.pitaya.entity.CarOrderRefEntity;
import top.pitaya.entity.OrderEntity;
import top.pitaya.entity.Page;
import top.pitaya.entity.UserOrderRefEntity;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {
    OrderEntity getOrderInfo(@Param("id") String id);

    Integer createOrder(OrderEntity orderEntity);

    Page getTotalPageAndSize(Map paramMap);

    List<OrderEntity> getOrderByPage(Map paramMap);

    int updateOrder(OrderEntity orderEntity);

    Integer deleteOrder(String id);

    Integer createUserOrderRef(UserOrderRefEntity userOrderRefEntity);

    Integer createCarOrderRef(CarOrderRefEntity carOrderRefEntity);

    Integer updateOrderStatus(Map param);
}
