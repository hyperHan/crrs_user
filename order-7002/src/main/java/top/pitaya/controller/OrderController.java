package top.pitaya.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.OrderEntity;
import top.pitaya.entity.Page;
import top.pitaya.service.OrderService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class OrderController {

    @Resource
    private OrderService orderService;

    /**
     * 订单管理、我的订单分页功能
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/order")
    public Map getOrderByPage(@RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                              @RequestParam(value = "fuzzy", defaultValue = "") String fuzzy,
                              @RequestParam(value = "id", defaultValue = "") String id) {
        log.info("参数currentPage：{}，pageSize：{}，fuzzy：{}, id：{}", currentPage, pageSize, fuzzy, id);
        Page<OrderEntity> page = orderService.getOrderByPage(currentPage, pageSize, fuzzy, id);
        Map result = new HashMap();
        result.put("data", page);
        if (page.getEntityList().size() > 0) {
            result.put("code", "200");
            result.put("msg", "查询成功！");
        } else {
            result.put("code", "444");
            result.put("msg", "未查询到相关数据！");
        }
        return result;
    }

    /**
     * Feign调用获取订单信息
     *
     * @param id
     * @return
     */
    @GetMapping("/order/{id}")
    public CommonResult<OrderEntity> getOrderInfo(@PathVariable("id") String id) {
        OrderEntity orderEntity = orderService.getOrderInfo(id);
        if (orderEntity != null) {
            return new CommonResult("200", "查询成功!", orderEntity);
        } else {
            return new CommonResult("444", "没有对应记录,查询ID: " + id);
        }
    }

    /**
     * 用户下单
     *
     * @param orderEntity
     * @return
     */
    @PostMapping(value = "/order")
    public CommonResult createOrder(@RequestBody OrderEntity orderEntity) {
        log.info("待插入的订单数据：" + orderEntity);
        return orderService.createOrder(orderEntity);
    }


    /**
     * 订单管理修改功能
     *
     * @param orderEntity
     * @return
     */
    @PutMapping(value = "/order")
    public CommonResult updateOrder(@RequestBody OrderEntity orderEntity) {
        log.info("待修改的订单数据：" + orderEntity);
        Integer count = orderService.updateOrder(orderEntity);
        if (count > 0) {
            return new CommonResult("200", "修改成功！");
        } else {
            return new CommonResult("444", "修改失败！");
        }
    }

    /**
     * 订单管理删除功能
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/order")
    public CommonResult deleteOrder(@RequestParam String id) {
        log.info("待删除的订单数据ID：" + id);
        Integer count = orderService.deleteOrder(id);
        if (count > 0) {
            return new CommonResult("200", "删除成功!");
        } else {
            return new CommonResult("444", "删除失败!");
        }
    }

    /**
     * 我的订单处，支付，取消，删除，评价的状态修改功能
     *
     * @param param
     * @return
     */
    @PutMapping(value = "/per/order")
    public CommonResult deletePerOrder(@RequestBody Map param) {
        Integer count = orderService.updateOrderStatus(param);
        if (count > 0) {
            return new CommonResult("200", "操作成功!");
        } else {
            return new CommonResult("444", "操作失败!");
        }
    }

}
