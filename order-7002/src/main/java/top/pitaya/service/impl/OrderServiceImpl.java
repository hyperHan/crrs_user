package top.pitaya.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.pitaya.dao.OrderMapper;
import top.pitaya.entity.*;
import top.pitaya.service.CarFeignService;
import top.pitaya.service.CommonService;
import top.pitaya.service.OrderService;
import top.pitaya.service.UserFeignService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private CommonService commonService;

    @Resource
    private CarFeignService carFeignService;

    @Resource
    private UserFeignService userFeignService;

    @Override
    public OrderEntity getOrderInfo(String id) {
        return orderMapper.getOrderInfo(id);
    }

    @Override
    public CommonResult createOrder(OrderEntity orderEntity) {
        String uuid = commonService.getUuid();
        orderEntity.setId(uuid);
        log.info("待插入订单数据ID：", uuid);
        // 新增订单记录
        Integer orderCount = orderMapper.createOrder(orderEntity);
        // 新增用户订单关系表
        Integer userRefCount = createUserOrderRef(uuid, orderEntity.getIdUser());
        // 新增汽车订单关系表
        Integer carRefCount = createCarOrderRef(uuid, orderEntity.getIdCar());
        if (orderCount > 0 && userRefCount > 0 && carRefCount > 0) {
            return new CommonResult("200", "新增成功！", uuid);
        } else {
            return new CommonResult("444", "新增失败");
        }
        // 调用RabbitMQ消息队列, 通知汽车服务，将库存减一
        // ......
    }

    // 新增汽车订单关系记录
    private Integer createCarOrderRef(String idOrder, String idCar) {
        CarOrderRefEntity carOrderRefEntity = new CarOrderRefEntity();
        String id = commonService.getUuid();
        carOrderRefEntity.setId(id);
        carOrderRefEntity.setIdOrder(idOrder);
        carOrderRefEntity.setIdCar(idCar);
        return orderMapper.createCarOrderRef(carOrderRefEntity);
    }

    // 新增用户订单关系记录
    private Integer createUserOrderRef(String idOrder, String idUser) {
        UserOrderRefEntity userOrderRefEntity = new UserOrderRefEntity();
        String id = commonService.getUuid();
        userOrderRefEntity.setId(id);
        userOrderRefEntity.setIdOrder(idOrder);
        userOrderRefEntity.setIdUser(idUser);
        return orderMapper.createUserOrderRef(userOrderRefEntity);
    }

    @Override
    public Page<OrderEntity> getOrderByPage(Integer currentPage, Integer pageSize, String fuzzy, String idUser) {
        Page<OrderEntity> page = new Page<>();
        page.setCurrentPage(currentPage);
        page.setPageSize(pageSize);
        // 获取总页数和总记录数
        Map paramMap = new HashMap();
        paramMap.put("pageSize", pageSize);
        paramMap.put("fuzzy", fuzzy);
        paramMap.put("idUser", idUser);
        Page pageTemp = orderMapper.getTotalPageAndSize(paramMap);
        page.setTotalSize(pageTemp.getTotalSize());
        page.setTotalPage(pageTemp.getTotalPage());
        // 获取分页列表
        paramMap.put("currentSize", (currentPage - 1) * pageSize);
        List<OrderEntity> list = orderMapper.getOrderByPage(paramMap);
        // 根据订单ID去关联表查询相关用户和汽车信息
        for (OrderEntity orderEntity : list) {
            // 获取用户服务 用户信息
             getUserInfo(orderEntity);
            // 获取汽车服务 汽车信息
            getCarInfo(orderEntity);
        }
        page.setEntityList(list);
        return page;
    }

    private void getCarInfo(OrderEntity orderEntity) {
        CommonResult<CarEntity> carResult = carFeignService.getCarInfo(orderEntity.getIdCar());
        CarEntity carEntity = carResult.getData();
        orderEntity.setBrand(carEntity.getBrand());
        orderEntity.setCarSeries(carEntity.getCarSeries());
        orderEntity.setCarType(carEntity.getCarType());
        orderEntity.setCarTypeName(carEntity.getCarTypeName());
        orderEntity.setGearbox(carEntity.getGearbox());
        orderEntity.setGearboxName(carEntity.getGearboxName());
        orderEntity.setSeats(carEntity.getSeats());
        orderEntity.setGears(carEntity.getGears());
        orderEntity.setSweptVolume(carEntity.getSweptVolume());
        orderEntity.setPrice(carEntity.getPrice());
        orderEntity.setColor(carEntity.getColor());
    }

    private void getUserInfo(OrderEntity orderEntity) {
        CommonResult<UserEntity> userResult = userFeignService.getUserInfo(orderEntity.getIdUser());
        UserEntity userEntity = userResult.getData();
        orderEntity.setUsername(userEntity.getUsername());
        orderEntity.setName(userEntity.getName());
        orderEntity.setTel(userEntity.getTel());
    }

    @Override
    public Integer updateOrder(OrderEntity orderEntity) {
        return orderMapper.updateOrder(orderEntity);
    }

    @Override
    public Integer deleteOrder(String id) {
        return orderMapper.deleteOrder(id);
    }

    @Override
    public Integer updateOrderStatus(Map param) {
        return orderMapper.updateOrderStatus(param);
    }
}
