package top.pitaya.service;

import top.pitaya.entity.CommonResult;
import top.pitaya.entity.OrderEntity;
import top.pitaya.entity.Page;

import java.util.Map;


public interface OrderService {
    OrderEntity getOrderInfo(String id);

    CommonResult createOrder(OrderEntity orderEntity);

    Page<OrderEntity> getOrderByPage(Integer currentPage, Integer pageSize, String fuzzy, String id);

    Integer updateOrder(OrderEntity orderEntity);

    Integer deleteOrder(String id);

    Integer updateOrderStatus(Map param);
}
