package top.pitaya.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import top.pitaya.service.OrderService;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author MR.HYPER
 * @version 1.0
 * @date 2022/04/12 1:39
 * @description
 */
@Slf4j
@Service
// @RabbitListener注解用于监听RabbitMQ，queues指定监听哪个队列
@RabbitListener(queues = "evaluationQueue")
public class Receiver {

    @Resource
    OrderService orderService;

    @RabbitHandler
    public boolean processOrderStatus(Map msg) {
        log.info("收到消息：{}", msg);
        if (msg != null) {
            orderService.updateOrderStatus(msg);
            return true;
        }
        return false;
    }

}
