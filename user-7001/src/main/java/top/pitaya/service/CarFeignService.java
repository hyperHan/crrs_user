package top.pitaya.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import top.pitaya.entity.CarEntity;
import top.pitaya.entity.Page;

import java.util.Map;


@Component
@FeignClient(value = "car-service")
public interface CarFeignService {
    // Feign解析不了参数，因此必须要指定value
    @GetMapping("/car")
    public Map getCarByPage(@RequestParam(value = "currentPage",defaultValue = "1") Integer currentPage,
                            @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                            @RequestParam(value = "fuzzy",defaultValue = "") String fuzzy);

}
