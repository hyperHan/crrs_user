package top.pitaya.service;

//import org.springframework.security.core.userdetails.UserDetails;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.Page;
import top.pitaya.entity.UserEntity;

import java.util.Map;


public interface UserService {
    UserEntity getUserById(String id);

    CommonResult createUser(UserEntity userEntity);

    Page<UserEntity> getUserByPage(Integer currentPage, Integer pageSize, String fuzzy);

    CommonResult updateUser(UserEntity userEntity);

    Integer deleteUser(String id);

    CommonResult updatePerBasicInfo(UserEntity userEntity);

    CommonResult updatePerPassword(Map param);
}
