package top.pitaya.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.pitaya.dao.UserMapper;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.Page;
import top.pitaya.entity.UserEntity;
import top.pitaya.service.CommonService;
import top.pitaya.service.UserService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private CommonService commonService;

    @Override
    public UserEntity getUserById(String id) {
        return userMapper.getUserById(id);
    }

    @Override
    public CommonResult createUser(UserEntity userEntity) {
        // user表主键
        String userId = commonService.getUuid();
        log.info("生成的userId:{}", userId);
        userEntity.setId(userId);
        int userCount = userMapper.createUser(userEntity);
        // ref表参数
        Map paramMap = new HashMap();
        String refId = commonService.getUuid();
        paramMap.put("refId", refId);
        paramMap.put("roleId", userEntity.getRole());
        paramMap.put("userId", userId);
        int refCount = userMapper.createRoleUserRef(paramMap);
        // 同时插入成功，返回成功
        if (userCount > 0 && refCount > 0) {
            return new CommonResult("200", "新增成功！");
        } else {
            return new CommonResult("444", "新增失败！");
        }
    }

    @Override
    public Page<UserEntity> getUserByPage(Integer currentPage, Integer pageSize, String fuzzy) {
        Page<UserEntity> page = new Page<>();
        page.setCurrentPage(currentPage);
        page.setPageSize(pageSize);
        // 获取总页数和总记录数
        Map paramMap = new HashMap();
        paramMap.put("pageSize", pageSize);
        paramMap.put("fuzzy", fuzzy);
        Page pageTemp = userMapper.getTotalPageAndSize(paramMap);
        page.setTotalSize(pageTemp.getTotalSize());
        page.setTotalPage(pageTemp.getTotalPage());
        // 获取分页列表
        paramMap.put("currentSize", (currentPage - 1) * pageSize);
        List<UserEntity> list = userMapper.getUserByPage(paramMap);
        page.setEntityList(list);
        return page;
    }

    @Override
    public CommonResult updateUser(UserEntity userEntity) {
        int userCount = userMapper.updateUser(userEntity);
        int refCount = userMapper.updateRoleUserRef(userEntity);
        if (userCount > 0 && refCount > 0) {
            return new CommonResult("200", "修改成功！");
        } else {
            return new CommonResult("444", "修改失败！");
        }
    }

    @Override
    public Integer deleteUser(String id) {
        return userMapper.deleteUser(id);
    }

    @Override
    public CommonResult updatePerBasicInfo(UserEntity userEntity) {
        int userCount = userMapper.updateUser(userEntity);
        if (userCount > 0) {
            return new CommonResult("200", "修改成功！");
        } else {
            return new CommonResult("444", "修改失败！");
        }
    }

    @Override
    public CommonResult updatePerPassword(Map param) {
        Integer count = userMapper.checkPassword(param);
        if (count > 0) {
            Integer result = userMapper.updatePerPassword(param);
            if (result > 0) {
                return new CommonResult("200", "密码修改成功!");
            } else {
                return new CommonResult("444", "密码修改失败!");
            }
        }
        return new CommonResult("442", "原始密码有误，请确认!");
    }

}
