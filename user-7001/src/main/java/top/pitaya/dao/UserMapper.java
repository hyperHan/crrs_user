package top.pitaya.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.pitaya.entity.Page;
import top.pitaya.entity.UserEntity;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {
    UserEntity getUserById(@Param("id") String id);

    int createUser(UserEntity userEntity);

    Page getTotalPageAndSize(Map paramMap);

    List<UserEntity> getUserByPage(Map paramMap);

    Integer updateUser(UserEntity userEntity);

    Integer deleteUser(String id);

    int createRoleUserRef(Map paramMap);

    int updateRoleUserRef(UserEntity userEntity);

    Integer updatePerPassword(Map param);

    Integer checkPassword(Map param);
}
