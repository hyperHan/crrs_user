package top.pitaya.controller;

import cn.hutool.db.sql.Order;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import top.pitaya.entity.*;
import top.pitaya.service.CarFeignService;
import top.pitaya.service.UserService;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@CrossOrigin
//@DefaultProperties(defaultFallback = "")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private CarFeignService carFeignService;

    /**
     * home首页 分页查询
     */
    @GetMapping("/home")
    public Map getHomeCarInfo(@RequestParam(defaultValue = "1") Integer currentPage) {
        // 调用汽车服务，获取首页汽车列表
        Map carMap = carFeignService.getCarByPage(currentPage, 10, "");
        // 调用猜你喜欢服务, 根据UserId查询猜你喜欢的汽车列表
        // String currentUserId = UserUtils.getCurrentUser().getId();
        // 模拟查询，后续改正==
        Map map = (Map) carMap.get("data");
        List entityList = (List) map.get("entityList");
        List<CarEntity> carEntityList = entityList.subList(0, 4);
        Map result = new HashMap();
        result.put("pageCar", map);
        result.put("userCar", carEntityList);
        // 模拟操作，后续改正==
        if (map != null && carEntityList.size() > 0) {
            result.put("code", "200");
            result.put("msg", "查询成功！");
        } else {
            result.put("code", "444");
            result.put("msg", "未查询到相关数据！");
        }
        return result;
    }

    /**
     * Feign调用获取用户信息、个人信息查询
     */
    @GetMapping("/user/{id}")
    public CommonResult<UserEntity> getUserInfo(@PathVariable("id") String id) {
        UserEntity userEntity = userService.getUserById(id);
        if (userEntity != null) {
            return new CommonResult("200", "查询成功!", userEntity);
        } else {
            return new CommonResult("444", "没有对应记录,查询ID: " + id);
        }
    }

    /**
     * 分页获取用户信息（支持模糊查询 username）
     */
    @GetMapping("/user")
    public Map getUserByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             @RequestParam(defaultValue = "") String fuzzy) {
        Page<UserEntity> page = userService.getUserByPage(currentPage, pageSize, fuzzy);
        Map result = new HashMap();
        result.put("data", page);
        if (page.getEntityList() != null && page.getEntityList().size() > 0) {
            result.put("code", "200");
            result.put("msg", "查询成功！");
        } else {
            result.put("code", "444");
            result.put("msg", "未查询到相关数据！");
        }
        return result;
    }

    /**
     * 创建用户信息
     */
    @PostMapping(value = "/user")
    public CommonResult createUser(@RequestBody UserEntity userEntity) {
        log.info("用户待插入的数据：" + userEntity);
        return userService.createUser(userEntity);
    }

    /**
     * 更新用户信息
     */
    @PutMapping("/user")
    public CommonResult updateUser(@RequestBody UserEntity userEntity) {
        return userService.updateUser(userEntity);
    }

    /**
     * 用户修改个人信息
     */
    @PutMapping("/per/basic")
    public CommonResult updatePerBasicInfo(@RequestBody UserEntity userEntity) {
        return userService.updatePerBasicInfo(userEntity);
    }

    /**
     * 用户修改个人信息（假删除）
     */
    @DeleteMapping("/user")
    public CommonResult deleteUser(@RequestParam String id) {
        Integer count = userService.deleteUser(id);
        if (count > 0) {
            return new CommonResult("200", "删除成功!");
        } else {
            return new CommonResult("444", "删除失败!");
        }
    }

    /**
     * 用户修改密码
     */
    @PutMapping("/per/reset")
    public CommonResult updatePerPassword(@RequestBody Map param) {
        return userService.updatePerPassword(param);
    }
}
