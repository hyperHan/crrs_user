package top.pitaya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
//@EnableEurekaClient
@EnableCaching
public class UserApplication7001 {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication7001.class, args);
    }

}
