package top.pitaya.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.pitaya.entity.CarEntity;
import top.pitaya.entity.Page;

import java.util.List;
import java.util.Map;

@Mapper
public interface CarMapper {
    CarEntity getCarInfo(@Param("id") String id);

    void createCar(CarEntity carEntity);

    List<CarEntity> getCarByPage(Map paramMap);

    Page getTotalPageAndSize(Map paramMap);

    Integer updateCar(CarEntity carEntity);

    Integer deleteCar(String id);
}
