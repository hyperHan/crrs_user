package top.pitaya.service;

import top.pitaya.entity.CarEntity;
import top.pitaya.entity.Page;



public interface CarService {
    CarEntity getCarInfo(String id);

    String createCar(CarEntity carEntity);

    Page<CarEntity> getCarByPage(Integer currentPage, Integer pageSize, String fuzzy);

    Integer updateCar(CarEntity carEntity);

    Integer deleteCar(String id);
}
