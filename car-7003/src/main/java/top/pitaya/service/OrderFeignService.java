package top.pitaya.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.OrderEntity;

@Component
@FeignClient(value = "order-service")
public interface OrderFeignService {
    @GetMapping("/order/get/{id}")
    public CommonResult<OrderEntity> getOrderInfo(@PathVariable("id") String id);//一定要加参数

}
