package top.pitaya.service.impl;

import org.springframework.stereotype.Service;
import top.pitaya.dao.CarMapper;
import top.pitaya.entity.CarEntity;
import top.pitaya.entity.Page;
import top.pitaya.service.CarService;
import top.pitaya.service.CommonService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarServiceImpl implements CarService {

    @Resource
    private CommonService commonService;

    @Resource
    private CarMapper carMapper;

    @Override
    public CarEntity getCarInfo(String id) {
        return carMapper.getCarInfo(id);
    }

    @Override
    public String createCar(CarEntity carEntity) {
        String uuid = commonService.getUuid();
        carEntity.setId(uuid);
        carMapper.createCar(carEntity);
        return uuid;
    }

    @Override
    public Page<CarEntity> getCarByPage(Integer currentPage, Integer pageSize, String fuzzy) {
        Page<CarEntity> page = new Page<>();
        page.setCurrentPage(currentPage);
        page.setPageSize(pageSize);
        // 获取总页数和总记录数
        Map paramMap = new HashMap();
        paramMap.put("pageSize", pageSize);
        paramMap.put("fuzzy", fuzzy);
        Page pageTemp = carMapper.getTotalPageAndSize(paramMap);
        page.setTotalSize(pageTemp.getTotalSize());
        page.setTotalPage(pageTemp.getTotalPage());
        // 获取分页列表
        paramMap.put("currentSize", (currentPage - 1) * pageSize);
        List<CarEntity> list = carMapper.getCarByPage(paramMap);
        page.setEntityList(list);
        return page;
    }

    @Override
    public Integer updateCar(CarEntity carEntity) {
        return carMapper.updateCar(carEntity);
    }

    @Override
    public Integer deleteCar(String id) {
        return carMapper.deleteCar(id);
    }

}
