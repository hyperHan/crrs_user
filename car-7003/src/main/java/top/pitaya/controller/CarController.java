package top.pitaya.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import top.pitaya.entity.CarEntity;
import top.pitaya.entity.CommonResult;
import top.pitaya.entity.OrderEntity;
import top.pitaya.entity.Page;
import top.pitaya.service.CarService;
import top.pitaya.service.OrderFeignService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@CrossOrigin
public class CarController {

    @Resource
    private CarService carService;

    /**
     * 汽车管理分页功能
     *
     * @param
     * @return
     */
    @GetMapping("/car")
    public Map getCarByPage(@RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                            @RequestParam(value = "fuzzy", defaultValue = "") String fuzzy) {
        // 获取汽车列表
        Page<CarEntity> page = carService.getCarByPage(currentPage, pageSize, fuzzy);
        Map result = new HashMap();
        result.put("data", page);
        if (page.getEntityList().size() > 0) {
            result.put("code", "200");
            result.put("msg", "查询成功！");
        } else {
            result.put("code", "444");
            result.put("msg", "未查询到相关数据！");
        }
        return result;
    }

    /**
     * Feign调用查询汽车详情
     *
     * @param id
     * @return
     */
    @GetMapping("/car/{id}")
    public CommonResult<CarEntity> getCarInfo(@PathVariable("id") String id) {
        CarEntity carEntity = carService.getCarInfo(id);
        if (carEntity != null) {
            return new CommonResult("200", "查询成功!", carEntity);
        } else {
            return new CommonResult("444", "没有对应记录,查询ID: " + id);
        }
    }

    /**
     * 汽车管理修改功能
     *
     * @param carEntity
     * @return
     */
    @PutMapping(value = "/car")
    public CommonResult updateCar(@RequestBody CarEntity carEntity) {
        log.info("待修改的汽车数据：" + carEntity);
        Integer count = carService.updateCar(carEntity);
        if (count > 0) {
            return new CommonResult("200", "修改成功！");
        } else {
            return new CommonResult("444", "修改失败");
        }
    }

    /**
     * 汽车管理新增功能
     *
     * @param carEntity
     * @return
     */
    @PostMapping(value = "/car")
    public CommonResult createCar(@RequestBody CarEntity carEntity) {
        log.info("待插入的汽车数据：" + carEntity);
        String carId = carService.createCar(carEntity);
        log.info("插入汽车数据的ID:", carId);
        if (carId != null) {
            return new CommonResult("200", "新增成功！", carId);
        } else {
            return new CommonResult("444", "新增失败");
        }
    }

    /**
     * 汽车管理删除功能
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/car")
    public CommonResult updateCar(@RequestParam String id) {
        log.info("待删除的汽车数据：" + id);
        Integer count = carService.deleteCar(id);
        if (count > 0) {
            return new CommonResult("200", "删除成功！");
        } else {
            return new CommonResult("444", "删除失败");
        }
    }

}
