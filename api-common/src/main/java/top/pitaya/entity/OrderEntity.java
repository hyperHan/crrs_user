package top.pitaya.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity implements Serializable {
    private String id;
    private String orderNo;
    private String paymentType;
    private String paymentTypeName;
    private BigDecimal actualPayment;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dateCreate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date datePayment;
    private String memo;
    private String status;
    private String statusName;
    private String idUser;
    private String idCar;

    // 以下做展示使用
    private String username;
    private String name;
    private String tel;
    private String brand;
    private String carSeries;
    private String carType;
    private String carTypeName;
    private String gearbox;
    private String gearboxName;
    private Integer seats;
    private String sweptVolume;
    private String color;
    private Integer gears;
    private BigDecimal price;
}
