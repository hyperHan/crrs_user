package top.pitaya.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationEntity implements Serializable {
    private String id;
    private String score;
    private String remark;
    private String idOrder;
    private String idCar;
    private String idUser;
    private String status;
    private String statusName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dateEvaluation;

    // 以下字段仅做展示
    private String username;
    private String name;
    private String brand;
    private String carSeries;
    private String carType;
    private String carTypeName;
    private String gearbox;
    private String gearboxName;
    private Integer seats;
    private String sweptVolume;
    private Integer gears;
    private BigDecimal price;
    private String orderNo;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;
}
