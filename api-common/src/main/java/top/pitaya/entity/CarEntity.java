package top.pitaya.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarEntity implements Serializable {
    private String id;
    private String carSeries;
    private String brand;
    private Integer doors;
    private Integer gears;
    private BigDecimal price;
    private Integer seats;
    private String color;
    private Integer stocks;
    private String status;
    private String statusName;
    private String sweptVolume;
    private String gearbox;
    private String gearboxName;
    private String carType;
    private String carTypeName;
    private String modelYear;
    private String image;
}
