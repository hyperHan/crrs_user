package top.pitaya.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity implements Serializable {
    private String id;
    private String username;
    private String password;
    private String name;
    private String tel;
    private String email;
    private String address;
    private String idNo;
    private String sex;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String status;
    private String statusName;
    private String role;
    private String roleName;

}
