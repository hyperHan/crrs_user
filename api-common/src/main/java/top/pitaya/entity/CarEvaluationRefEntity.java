package top.pitaya.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarEvaluationRefEntity implements Serializable {
    private String id;
    private String idCar;
    private String idEvaluation;
}
