package top.pitaya.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T> {
    private String code;
    private String msg;
    private T data;

    /**
     * T是各个模块返回的entity,当T为空时调用以下方法
     *
     * @param code
     * @param msg
     */
    public CommonResult(String code, String msg) {
        this(code, msg, null);
    }
}
