package top.pitaya.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> implements Serializable {
    // 当前页
    private Integer currentPage;
    // 每页记录数
    private Integer pageSize;
    // 总记录数
    private Integer totalSize;
    // 总页数
    private Integer totalPage;
    // 当前页实体数
    private List<T> entityList;
}
