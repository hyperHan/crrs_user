package top.pitaya.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEvaluationRefEntity implements Serializable {
    private String id;
    private String idUser;
    private String idOrder;
}
