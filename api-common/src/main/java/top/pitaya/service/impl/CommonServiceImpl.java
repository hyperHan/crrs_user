package top.pitaya.service.impl;

import org.springframework.stereotype.Service;
import top.pitaya.service.CommonService;

import java.util.UUID;

@Service
public class CommonServiceImpl implements CommonService {

    @Override
    public String getUuid() {
        return UUID.randomUUID().toString();
    }

}
